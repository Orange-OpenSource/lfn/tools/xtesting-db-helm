# xTesting-db-helm

This repository handle helm charts to deploy DB services for xTesting

## s3

* **s3www** service to display reports (s3 frontend)
* **s3** service handled by minio to store reports [optional]

[More information in the chart repository](s3/README.md)

## testApi

* **testapi** service that handle API for tests result storage
* **mongodb** service to store results

[More information in the chart repository](testapi/README.md)

## helm repository

Add the repo
```
helm repo add xtesting https://orange-opensource.gitlab.io/lfn/tools/xtesting-db-helm/
```

Once the repo installed, you may find the charts as follows:
```
helm search repo xtesting
NAME                         	CHART VERSION	APP VERSION	DESCRIPTION
xtesting-testapi-repo/s3www  	0.2.5        	           	A Helm chart to deploy xTesting s3 applications
xtesting-testapi-repo/testapi	0.3.0       	           	A Helm chart to deploy xTesting TestAPI
```
