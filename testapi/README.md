
## Introduction

This chart bootstraps testAPI and MongoDB for xTesting. It includes:
- Mongo&reg; server
- testAPI server

## Installing the Chart

To install the chart with the release name `xtesting-testapi`:

```bash
$ helm install xtesting-testapi ./
```

## Parameters

### Global parameters

| Parameter                       | Description                                     | Default                                           |
|---------------------------------|-------------------------------------------------|---------------------------------------------------|
| `image.nameOverride`            | force testapi name                              | `nil`                                             |
| `image.fullnameOverride`        | force testapi fullname                          | `nil`                                             |

### s3www image parameters

| Parameter                       | Description                                     | Default                                           |
|---------------------------------|-------------------------------------------------|---------------------------------------------------|
| `image.registry`                | Docker registry                                 | `docker.io`                                       |
| `image.repository`              | Docker image name                               | `ollivier/xtesting-s3www`                         |
| `image.tag`                     | Docker image tag                                | `latest`                                          |
| `image.pullPolicy`              | Pull policy                                     | `IfNotPresent`                                    |

### Exposure parameters

| Parameter                          | Description                                            | Default                                 |
|------------------------------------|--------------------------------------------------------|-----------------------------------------|
| `service.openshiftRoute`           | use openShift route to expose the services             | `false`                                 |
| `service.hostUrl`                  | baseUrl used to expose the services                    | `my.route.base.url`                     |
| `service.type`                     | Kubernetes service type                                | `ClusterIP`                             |
| `service.port`                     | s3www service port                                     | `8080`                                  |
| `service.nodePort`                 | Port to bind to for NodePort service type              | `nil`                                   |
| `service.externalTrafficPolicy`    | Enable client source IP preservation                   | `Cluster`                               |
| `service.loadBalancerIP`           | loadBalancerIP if service type is `LoadBalancer`       | `nil`                                   |
| `service.loadBalancerSourceRanges` | Address that are allowed when service is LoadBalancer  | `[]`                                    |

### Security contexts

| Parameter                                | Description                                     | Default                                  |
|------------------------------------------|-------------------------------------------------|------------------------------------------|
| `podSecurityContext.enabled`             | enable pod security context                     | `true`                                   |
| `containerSecurityContext.enabled`       | enable container security context               | `true`                                   |
| `rbac.enabled`                           | create role and role binding                    | `true`                                   |
| `rbac.role.rules`                        | default role rules                              |                                          |
| `serviceAccount.enabled`                 | create service account                          | `true`                                   |
| `serviceAccount.name`                    | serviceAccountname, if not set fullname is used |                                          |

The values used will be the one given in `containerSecurityContext` and `containerSecurityContext` dictionnary.
You can add / remove / change these values as every other values.

Here are the two default values:

```yaml
podSecurityContext:
  runAsNonRoot: true
  runAsUser: &user_id 1005360001
  runAsGroup: *user_id
  fsGroup: *user_id
  sysctls: []
```

```yaml
containerSecurityContext:
  allowPrivilegeEscalation: false
```

Here are the default rbac rules for the role:

```yaml
 role:
   ## Rules to be created depending on the role
   rules:
   - apiGroups:
     - ''
     resources:
     - services
     - endpoints
     - pods
     verbs:
     - get
     - watch
     - list
```

### Mongo parameters

| Parameter                                | Description                                     | Default                                  |
|------------------------------------------|-------------------------------------------------|------------------------------------------|
| `mongo.auth.disabled`                    | Disable mongo authentication                    | `true`                                   |
| `mongo.service.port`                     | Mongo default port                              | `27017`                                  |


### Deactivate Mongo security context for openshift

| Parameter                       | Description                                     | Default                                           |
|---------------------------------|-------------------------------------------------|---------------------------------------------------|
| `mongo.podSecurityContext.enabled`       | Enable security context at pod level            | `false` (for openShift compatibility)    |
| `mongo.containerSecurityContext.enabled` | Enable security context at container level      | `false` (for openShift compatibility)    |

## Values examples

### Deploy mongodb and testapi without values

```sh
helm upgrade -i xtesting-testapi testapi
Release "xtesting-testapi" does not exist. Installing it now.
NAME: xtesting-testapi
LAST DEPLOYED: Tue May  4 17:49:05 2021
NAMESPACE: neo
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
###############################################################################

Thank you for installing testapi.
Your release is named xtesting-testapi.

Mongo and TestAPI are now deployed.

xTesting parameters are:


TEST_DB_URL: http://xtesting-testapi.8000/api/v1/results

###############################################################################
```

### Deploy mongodb and testapi with OpenShift route

s3_values.yaml:
```yaml
------
service:
  openshiftRoute: true
  hostUrl: myopenshift.base.route
minio:
  securityContext:
    enabled: false
  containerSecurityContext:
    enabled: false
```

```sh
helm upgrade -i -f testapi_values.yml xtesting-testapi testapi
Release "xtesting-testapi" has been upgraded. Happy Helming!
NAME: xtesting-testapi
LAST DEPLOYED: Tue May  4 17:50:27 2021
NAMESPACE: neo
STATUS: deployed
REVISION: 2
TEST SUITE: None
NOTES:
###############################################################################

Thank you for installing testapi.
Your release is named xtesting-testapi.

Mongo and TestAPI are now deployed.

xTesting parameters are:


TEST_DB_URL: https://xtesting-testapi-mynamespace.myopenshift.base.route/api/v1/results

###############################################################################
