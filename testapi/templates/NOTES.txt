###############################################################################

Thank you for installing {{ .Chart.Name }}.
Your release is named {{ .Release.Name }}.

Mongo and TestAPI are now deployed.

xTesting parameters are:

{{ if .Values.service.openshiftRoute }}
TEST_DB_URL: https://{{ .Release.Name }}-{{ .Release.Namespace }}.{{ .Values.service.hostUrl }}/api/v1/results
{{- else }}
TEST_DB_URL: http://{{ template "testapi.fullname" . }}.{{ .Values.service.port }}/api/v1/results
{{- end }}

###############################################################################